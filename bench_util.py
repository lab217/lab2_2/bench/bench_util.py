import random
import string


def create_lst(length):
        lst = [( random.randint(1, 100)) for i in range( length+1)]
        return lst
gg = (create_lst(319))
# print(len(gg))

def generate_random_string(i):
    letters = string.ascii_uppercase[0:26]
    rand_string = ''.join(random.choice(letters) for i in range(i))
    # print("Random string of length", length, "is:", rand_string)
    return rand_string


def correct_lines(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines ]
    for line in lines_1:
        if line.find('swap') != -1:
            result = line.split(' ')[0]
            break
    return result

